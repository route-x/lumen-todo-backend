<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TodoTest extends TestCase
{
    /** 
     * /todos [GET] 
     */
    public function testReturnAllTodos()
    {
        $this->get("todos", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            '*' => [
                'id', 'name', 'status'
            ]
        ]);
    }

    /**  
     * /todos [POST] 
     */
    public function testCreateTodo()
    {
        $this->json('POST', '/todos', [
                    'name' => 'Make a helicopter',
                    'status' => 'to-do'
                ])
             ->seeJson(['created' => true]);
    }

    /** 
     * /todos [GET] 
     */
    public function testReturnTodoById()
    {
        $this->get("todos/1", []);
        $this->seeStatusCode(200);
    }

    /** 
     * /todos [PUT] 
     */
    public function testUpdateTodo()
    {
        $this->json('PUT', "/todos/1", [
                    'name' => 'Make a pc',
                    'status' => 'to-do'
                ])
             ->seeJson(['updated' => true]);
    }

    /**
     * /todos/id [DELETE]
     */
    public function testDeleteTodo()
    {    
        $this->json('DELETE', "/todos/3",[])
             ->seeJson(['deleted' => true]);
    }
}
