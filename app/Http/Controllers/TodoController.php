<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function index()
    {
        return response()->json(Todo::orderBy('id', 'DESC')->get());
    }
    
    public function store(Request $request)
    {
        Todo::create($request->all());

        return (['created' => true]);
    }

    public function show($id) 
    { 
        $todo = Todo::find($id);

        if ($todo == null) return (['todo' => null]);

        return response()->json($todo);
    }

    public function update(Request $request, $id)
    {
        Todo::find($id)->update($request->all());

        return (['updated' => true]);
    }

    public function delete($id)
    {
        Todo::destroy($id);

        return (['deleted' => true]);
    }
}
